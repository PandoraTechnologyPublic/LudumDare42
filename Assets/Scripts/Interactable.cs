﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : ManagedComponent
{
    protected bool _canGrab = false;
    
    public abstract void Interact();
    public abstract void ToggleHighlight(bool toggle);

    public virtual float GetItemMass()
    {
        return 0;
    }

    public virtual void ThrowItem(float force)
    {

    }

    public bool CanGrab()
    {
        return _canGrab;
    }
}
