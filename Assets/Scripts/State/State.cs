﻿using UnityEngine;

public abstract class State : MonoBehaviour
{
    public abstract void OnEnter(GameManager gameManager);
    public abstract void OnUpdate(GameManager gameManager);
    public abstract void OnExit(GameManager gameManager);
}
