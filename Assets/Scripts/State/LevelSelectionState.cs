﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectionState : State
{
    private GameManager _gameManager;
    
    public override void OnEnter(GameManager gameManager)
    {
        _gameManager = gameManager;
        gameManager.GetGuiManager().ShowLevelSelectionScreen();
        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
    }

    private void SceneManagerOnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        SceneManager.SetActiveScene(scene);
        _gameManager.ChangeState<IngameState>(); 
    }

    public override void OnUpdate(GameManager gameManager)
    {
        
    }

    public override void OnExit(GameManager gameManager)
    {
        SceneManager.sceneLoaded -= SceneManagerOnSceneLoaded;
        
        gameManager.SetLocalPlayerName(gameManager.GetGuiManager().GetLocalPlayerNameInputFieldValue());
    }
}
