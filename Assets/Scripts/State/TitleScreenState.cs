﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenState : State
{
    public override void OnEnter(GameManager gameManager)
    {
        gameManager.GetGuiManager().ShowTitleScreen();

        //gameManager.LoadScene("testLevel");
    }

    public override void OnUpdate(GameManager gameManager)
    {
        if (Input.GetButtonDown("Interact"))
        {
            gameManager.ChangeState<LevelSelectionState>();
        }
    }

    public override void OnExit(GameManager gameManager)
    {
        
    }
}
