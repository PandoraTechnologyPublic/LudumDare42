﻿using System.Threading;
using UnityEngine;


    public class IngameState : State
    {
        private float _timeSinceStart;
        private GameManager _gameManager;
        private bool _isPaused = false;
        
        public override void OnEnter(GameManager gameManager)
        {
            _gameManager = gameManager;
            _timeSinceStart = 0;
            _gameManager.Initialise();
            _gameManager.GetGuiManager().ShowIngameScreen();
            _gameManager.GetCurrentLevel().EnableCrateSpawning();
        }

        public override void OnUpdate(GameManager gameManager)
        {
            float levelDuration = gameManager.GetCurrentLevel().GetLevelDuration();
            
            _timeSinceStart += Time.deltaTime;

            if (Input.GetButtonDown("Pause"))
            {
                TogglePause();
            }

            if (levelDuration - _timeSinceStart < 30)
            {
                gameManager.GetGuiManager().SetCountdownColor(Color.red);
                gameManager.GetCurrentLevel().SpeedupMusic();
            }
            else
            {
                gameManager.GetGuiManager().SetCountdownColor(Color.white);
            }
            
            gameManager.GetGuiManager().SetGameplayPhase(
                string.Format(
                    "{0:00}:{1:00}", 
                    Mathf.FloorToInt((levelDuration - _timeSinceStart) / 60), 
                    Mathf.FloorToInt(levelDuration - _timeSinceStart) % 60
                )
            );

            if (_timeSinceStart >= levelDuration)
            {
                gameManager.ChangeState<GameOverState>();
            }
        }

        public override void OnExit(GameManager gameManager)
        {
            gameManager.GetCurrentLevel().DisableCrateSpawning();
        }

        public void PauseGame()
        {
            Time.timeScale = 0;
        }

        public void TogglePause()
        {
            if (!_isPaused)
            {
                _gameManager.GetGuiManager().ShowPauseMenuScreen();
                Invoke("PauseGame", 0.75f);
                _isPaused = true;
            }
            else
            {
                _gameManager.GetGuiManager().ShowIngameScreen();
                Time.timeScale = 1;
                CancelInvoke();
                _isPaused = false;
            }
        }
    }
