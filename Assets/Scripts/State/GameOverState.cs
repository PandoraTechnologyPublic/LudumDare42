﻿using UnityEngine;

public class GameOverState : State
{
    public override void OnEnter(GameManager gameManager)
    {
        int score = gameManager.GetScore();
        int starCount = gameManager.GetStarCount(score);

        gameManager.SaveScore();

        if (starCount > 0)
        {
            gameManager.UnlockNextLevel();
        }
        
        gameManager.GetGuiManager().DisplayStars(starCount);
        gameManager.GetGuiManager().DisplayScore(score);
        
        gameManager.GetGuiManager().ShowGameOverScreen();
    }

    public override void OnUpdate(GameManager gameManager)
    {
        if (Input.GetButtonDown("Interact") &&
            gameManager.GetGuiManager().GetGameOverAnimator()
                .GetCurrentAnimatorStateInfo(0).IsName("PressSpaceAnimation"))
        {
            gameManager.ChangeState<LevelSelectionState>();
        }

        if (gameManager.GetGuiManager().IsLeaderbourdDisplayed() && Input.GetButtonDown("Pause"))
        {
            gameManager.GetGuiManager().ShowGameOverScreen();
        }
    }

    public override void OnExit(GameManager gameManager)
    {
    }
}
