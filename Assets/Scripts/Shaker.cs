﻿using UnityEngine;

public class Shaker : MonoBehaviour {

    [SerializeField] private float _speed = 1.0f; //how fast it shakes
    [SerializeField] private float _amount = 1.0f; //how much it shakes

    private bool _isShaking;
    private float _startingTime;

    public void startShaking()
    {
        _isShaking = true;
        _startingTime = Time.time;
    }

    public void stopShaking()
    {
        _isShaking = false;
    }

    void Update()
    {
        if (_isShaking)
        {
            float deltaTime = Time.time - _startingTime;

            float x = Mathf.Sin(deltaTime * Random.value * _speed) * _amount;
            float y = Mathf.Sin(deltaTime * Random.value * _speed) * _amount;
            float z = Mathf.Sin(deltaTime * Random.value * _speed) * _amount;
            transform.position = new Vector3(x, y, z);

            /*
            x = Mathf.Sin(deltaTime * _speed) * _amount;
            y = Mathf.Sin(deltaTime * _speed) * _amount;
            z = Mathf.Sin(deltaTime * _speed) * _amount;
            transform.rotation = new Quaternion(x, y, z, transform.rotation.w);
            */
        }
    }
}
