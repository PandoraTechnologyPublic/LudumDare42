﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
	[SerializeField] private PlayerController _playerPrefab;

	private PlayerController _playerEntity;

	public void Respawn()
	{
		if (_playerEntity != null)
		{
			GameObject.Destroy(_playerEntity);
		}

		_playerEntity = GameObject.Instantiate(_playerPrefab, transform.position, transform.rotation);
	}
}
