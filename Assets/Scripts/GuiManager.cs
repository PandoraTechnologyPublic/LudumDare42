﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class GuiManager : ManagedComponent
{
    [SerializeField] private RectTransform _titleScreen;
    [SerializeField] private RectTransform _ingameScreen;
    [SerializeField] private RectTransform _levelSelectionScreen;
    [SerializeField] private RectTransform _gameOverScreen;
    [SerializeField] private RectTransform _pauseMenuScreen;
    [SerializeField] private RectTransform _leaderboardScreen;
    [SerializeField] private Image _oneStarImage;
    [SerializeField] private Image _twoStarImage;
    [SerializeField] private Image _threeStarImage;
    
    [SerializeField] private LevelButtonLayout _levelButtonPrefab;
    [SerializeField] private RectTransform _levelButtonDock;
    
    [SerializeField] private TMP_Text _gameplayPhaseText;
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private TMP_Text _gameOverScreenScoreText;
    
    [SerializeField] private Animator _gameOverAnimator;

    [SerializeField] private Button _continueButton;
    [SerializeField] private Button _levelSelectionButton;
    [SerializeField] private Button _leaderboardButton;
    
    [SerializeField] private TMP_InputField _playerNameInputField;
    
    [SerializeField] private RectTransform _leaderboardEntryDock;
    [SerializeField] private LeaderboardEntryLayout _leaderboardEntryPrefab;
    [SerializeField] private int maxLeaderboardEntryCount = 12;
    
    private readonly Dictionary<string, LevelButtonLayout> _levelButtons = new Dictionary<string, LevelButtonLayout>();
    private readonly List<LeaderboardEntryLayout> _leaderboardEntryLayouts = new List<LeaderboardEntryLayout>();
    
    private void Start()
    {
        GetGameManager().onScoreChanged += SetScore;
        GetGameManager().onGameplayPhaseChanged += SetGameplayPhase;
        _continueButton.onClick.AddListener(OnContinueButtonClick);
        _levelSelectionButton.onClick.AddListener(OnLevelSelectionButtonClick);
        _leaderboardButton.onClick.AddListener(OnLeaderboardButtonClick);
    }

    private void OnLeaderboardButtonClick()
    {
        ShowLeaderboardScreen();
        UpdateLeaderboard();
    }

    private void UpdateLeaderboard()
    {
        if (_leaderboardEntryLayouts.Count <= 0)
        {
            for (int leaderboardIndex = 0; leaderboardIndex < maxLeaderboardEntryCount; leaderboardIndex++)
            {
                _leaderboardEntryLayouts.Add(GameObject.Instantiate(_leaderboardEntryPrefab,_leaderboardEntryDock));
            }
        }

        StartCoroutine(GetGameManager().GetComponentInChildren<LeaderboardService>(true).searchScores(OnScoreReceived, GetGameManager().GetCurrentLevel().name, 0 , maxLeaderboardEntryCount));
    }

    private void OnScoreReceived(List<LeaderboardService.Score> scores)
    {
        foreach (LeaderboardEntryLayout leaderboardEntryLayout in _leaderboardEntryLayouts)
        {
            leaderboardEntryLayout.gameObject.SetActive(false); 
        }
        
        for (int scoreIndex = 0; scoreIndex < scores.Count; scoreIndex++)
        {
            _leaderboardEntryLayouts[scoreIndex].SetData(scores[scoreIndex].playerName, scores[scoreIndex].score.ToString("D"));
            _leaderboardEntryLayouts[scoreIndex].gameObject.SetActive(true);
        }
    }

    private void OnLevelSelectionButtonClick()
    {
        if (GetGameManager().IsCurrentState<IngameState>())
        {
            IngameState currentState = GetGameManager().GetCurrentState<IngameState>();

            currentState.TogglePause();
            
            GetGameManager().ChangeState<LevelSelectionState>();
        }
    }

    private void OnContinueButtonClick()
    {
        if (GetGameManager().IsCurrentState<IngameState>())
        {
            IngameState currentState = GetGameManager().GetCurrentState<IngameState>();

            currentState.TogglePause();
        }
    }

    private void OnLevelButtonClick(string levelName)
    {
        _levelSelectionScreen.gameObject.SetActive(false);
        GetGameManager().LoadScene(levelName);
    }

    public void ShowTitleScreen()
    {
        _titleScreen.gameObject.SetActive(true);
        _ingameScreen.gameObject.SetActive(false);
        _levelSelectionScreen.gameObject.SetActive(false);
        _gameOverScreen.gameObject.SetActive(false);
        _pauseMenuScreen.gameObject.SetActive(false);
    }

    public void ShowIngameScreen()
    {
        _titleScreen.gameObject.SetActive(false);
        _ingameScreen.gameObject.SetActive(true);
        _levelSelectionScreen.gameObject.SetActive(false);
        _gameOverScreen.gameObject.SetActive(false);
        _pauseMenuScreen.gameObject.SetActive(false);
        _leaderboardScreen.gameObject.SetActive(false);
    }

    public void ShowLeaderboardScreen()
    {
        _titleScreen.gameObject.SetActive(false);
        _ingameScreen.gameObject.SetActive(false);
        _levelSelectionScreen.gameObject.SetActive(false);
        _gameOverScreen.gameObject.SetActive(false);
        _pauseMenuScreen.gameObject.SetActive(false);
        _leaderboardScreen.gameObject.SetActive(true);
    }

    public void ShowPauseMenuScreen()
    {
        _titleScreen.gameObject.SetActive(false);
        _ingameScreen.gameObject.SetActive(false);
        _levelSelectionScreen.gameObject.SetActive(false);
        _gameOverScreen.gameObject.SetActive(false);
        _pauseMenuScreen.gameObject.SetActive(true);
        _leaderboardScreen.gameObject.SetActive(false);
        
        _continueButton.Select();
    }

    public void ShowLevelSelectionScreen()
    {
        _titleScreen.gameObject.SetActive(false);
        _ingameScreen.gameObject.SetActive(false);
        _levelSelectionScreen.gameObject.SetActive(true);
        _gameOverScreen.gameObject.SetActive(false);
        _pauseMenuScreen.gameObject.SetActive(false);
        _leaderboardScreen.gameObject.SetActive(false);

        UpdateLevelSelectionScreen();

        _playerNameInputField.text = GetGameManager().GetLocalPlayerName();
    }

    public void UpdateLevelSelectionScreen()
    {
        foreach (string levelName in GetGameManager().GetLevels())
        {
            LevelButtonLayout levelButton;
            
            if (!_levelButtons.ContainsKey(levelName))
            {
                levelButton = GameObject.Instantiate(_levelButtonPrefab, _levelButtonDock); 
                
                levelButton.GetComponent<Button>().onClick.AddListener(() => OnLevelButtonClick(levelName));
                _levelButtons.Add(levelName, levelButton);
            }

            levelButton = _levelButtons[levelName];
            
            bool isLevelUnlocked = GetGameManager().IsLevelUnlocked(levelName);
            
            levelButton.GetComponent<Button>().interactable = isLevelUnlocked;
            
            if (isLevelUnlocked)
            {
                levelButton.GetComponent<Button>().Select();
            }

            levelButton.SetLevelName(levelName);
            levelButton.DisplayStars(GetGameManager().GetStars(levelName));
            levelButton.DisplayScore(GetGameManager().GetScore(levelName));
        }
    }

    public void ShowGameOverScreen()
    {
        _titleScreen.gameObject.SetActive(false);
        _ingameScreen.gameObject.SetActive(false);
        _levelSelectionScreen.gameObject.SetActive(false);
        _gameOverScreen.gameObject.SetActive(true);
        _pauseMenuScreen.gameObject.SetActive(false);
        _leaderboardScreen.gameObject.SetActive(false);

        // Removes focus from Leaderboard button (space bar would re-open leaderboard instead of level select)
        FindObjectOfType<EventSystem>().SetSelectedGameObject(_scoreText.gameObject);
    }

    public void DisplayStars(int starCount)
    {
        _oneStarImage.gameObject.SetActive(false);
        _twoStarImage.gameObject.SetActive(false);
        _threeStarImage.gameObject.SetActive(false);
        
        if (starCount >= 1)
        {
            _oneStarImage.gameObject.SetActive(true);
            
            if (starCount >= 2)
            {
                _twoStarImage.gameObject.SetActive(true);
                
                if (starCount >= 3)
                {
                    _threeStarImage.gameObject.SetActive(true);
                }
            }
        }
    }

    public void SetGameplayPhase(string gameplayPhaseText)
    {
        _gameplayPhaseText.SetText(gameplayPhaseText);
    }

    public void SetCountdownColor(Color color)
    {
        _gameplayPhaseText.color = color;
    }

    private void SetScore(int score)
    {
        _scoreText.SetText(string.Format("Score : {0}", score));
    }

    public void DisplayScore(int score)
    {
        _gameOverScreenScoreText.SetText(string.Format("Your score : {0}", score));
    }

    public Animator GetGameOverAnimator()
    {
        return _gameOverAnimator;
    }

    public bool IsLeaderbourdDisplayed()
    {
        return _leaderboardScreen.gameObject.activeSelf;
    }

    public string GetLocalPlayerNameInputFieldValue()
    {
        return _playerNameInputField.text;
    }
}