﻿using UnityEngine;

public class PushButton : MonoBehaviour
{
    [SerializeField] private GameObject buttonGameObject;
    [SerializeField] private Animator door;

    public bool _isDoorOpen = false;

    private Rigidbody buttonRigidbody;

    private void Awake()
    {
        buttonRigidbody = buttonGameObject.GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject == buttonGameObject)
        {
            //LockButton();
            door.SetTrigger("Button");
            _isDoorOpen = !_isDoorOpen;
            print(_isDoorOpen);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject == buttonGameObject)
        {
            //Toggle(false);
        }
    }


    public void UnlockButton()
    {
        buttonRigidbody.isKinematic = false;
    }

    public void LockButton()
    {
        buttonRigidbody.isKinematic = true;
    }
}
