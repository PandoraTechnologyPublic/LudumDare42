﻿using UnityEngine;

public class ManagedComponent : MonoBehaviour
{
    private GameManager _gameManager;

    protected GameManager GetGameManager()
    {
            if (_gameManager == null)
            {
                _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            }
            return _gameManager;
    }
}