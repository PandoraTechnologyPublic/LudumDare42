﻿using UnityEngine;

public class PlayerItemSelector : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private bool _addItemMass = true;
    
    private Interactable _selectedItem;
    private Interactable _heldItem;
    private bool _isHoldingItem = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!_selectedItem && !_isHoldingItem)
        {
            Interactable itemComponent = other.GetComponent<Interactable>();
            if (itemComponent == null)
            {
                itemComponent = other.GetComponentInParent<Interactable>();
            }
            if (itemComponent)
            {
                _selectedItem = itemComponent;
                _selectedItem.ToggleHighlight(true);
            }
        }
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (!_selectedItem && !_isHoldingItem)
        {
            Interactable itemComponent = other.GetComponent<Interactable>();
            if (itemComponent == null)
            {
                itemComponent = other.GetComponentInParent<Interactable>();
            }
            if (itemComponent)
            {
                _selectedItem = itemComponent;
                _selectedItem.ToggleHighlight(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_selectedItem)
        {
            Interactable itemComponent = other.GetComponent<Interactable>();
            if (itemComponent == null)
            {
                itemComponent = other.GetComponentInParent<Interactable>();
            }
            if (itemComponent == _selectedItem)
            {
                _selectedItem.ToggleHighlight(false);
                _selectedItem = null;
            }
        }
    }

    public Interactable GetSelectedBox()
    {
        return _selectedItem;
    }

    public void GrabItem()
    {
        if (_selectedItem && !_heldItem)
        {
            _heldItem = _selectedItem;
            _heldItem.Interact();
            _heldItem.transform.rotation = transform.rotation;
            _heldItem.transform.position = transform.position;
            _heldItem.transform.SetParent(transform);

            if (_addItemMass)
            {
                _rigidbody.mass += _heldItem.GetItemMass() / 10;
            }

            _isHoldingItem = true;
        }
    }

    public void ThrowItem(float force)
    {
        if (_heldItem && _isHoldingItem)
        {
            Debug.Log("Releasing...");
            _heldItem.transform.SetParent(null);
            _heldItem.Interact();
            
            if (_addItemMass)
            {
                _rigidbody.mass -= _heldItem.GetItemMass() / 10;
            }
            
            _heldItem.ThrowItem(force);
            
            _heldItem = null;
            _isHoldingItem = false;
        }
    }

    public void ReleaseItem()
    {
        if (_heldItem && _isHoldingItem)
        {
            Debug.Log("Releasing...");
            _heldItem.transform.SetParent(null);
            _heldItem.Interact();
            
            if (_addItemMass)
            {
                _rigidbody.mass -= _heldItem.GetItemMass() / 10;
            }
            
            _heldItem = null;
            _isHoldingItem = false;
        }
    }

    public void ItemInteract()
    {
        if (_isHoldingItem)
        {
            ReleaseItem();
        }
        else if(_selectedItem && _selectedItem.CanGrab())
        {
            GrabItem();
        }
        else if (_selectedItem)
        {
            _selectedItem.Interact();
        }
    }
}