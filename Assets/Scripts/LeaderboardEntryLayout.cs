﻿using TMPro;
using UnityEngine;

public class LeaderboardEntryLayout : MonoBehaviour
{
    [SerializeField] private TMP_Text playerNameText;
    [SerializeField] private TMP_Text playerScoreText;

    public void SetData(string playerName, string playerScore)
    {
        playerNameText.SetText(playerName);
        playerScoreText.SetText(playerScore);
    }
}
