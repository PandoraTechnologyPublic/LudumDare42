﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private PlayerItemSelector _itemSelector;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _breakForce;
    [SerializeField] private float _maxMoveSpeed = 5;
    [SerializeField] private float _dashForce = 10;
    //[SerializeField] private float _dashDuration = 0.5f;
    [SerializeField] private float _throwingForce = 5;
    [SerializeField] private float _dashInterval = 1.0f;
    [SerializeField] private Rigidbody _rigidbody;

    private Box _selectedBox;
    private float _dashTime = 0f;
    private Vector3 _dashDirection;
    private float _currentMaxMoveSpeed;

    private void Start()
    {
        _rigidbody.freezeRotation = true;
    }
    
    private void Update()
    {
        UpdateControls();
    }

    private void UpdateControls()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (horizontal != 0 || vertical != 0)
        {
            Vector3 direction = new Vector3(horizontal, 0, vertical);
            _rigidbody.MoveRotation(Quaternion.RotateTowards(_rigidbody.rotation,
                Quaternion.Euler(0, Vector3.SignedAngle(Vector3.forward, direction, Vector3.up), 0),
                1000f * Time.deltaTime));
            //_rigidbody.velocity = new Vector3(horizontal * _moveSpeed, _rigidbody.velocity.y, vertical * _moveSpeed);

            if (direction.magnitude > 1)
            {
                direction.Normalize();
            }
            
            _rigidbody.AddForce(direction * _moveSpeed);
            //_rigidbody.MovePosition(transform.position + direction * Time.deltaTime * _moveSpeed);
            //_rigidbody.velocity = Vector3.zero;

            _dashTime += Time.deltaTime;
            
            if (_rigidbody.velocity.magnitude > _maxMoveSpeed)
            {
                _rigidbody.AddForce(_breakForce * -1 * _rigidbody.velocity.normalized);
            }
            
            /*if (_dashTime < _dashInterval)
            {
                if (_dashTime < _dashDuration)
                {
                    _rigidbody.velocity += _dashDirection * (_dashForce * (_dashDuration - _dashTime));
                    _dashTime += Time.deltaTime;
                }
            }*/
            if (Input.GetButtonDown("Dash") && _dashTime >= _dashInterval)
            {
                _dashTime = 0;
                //_dashDirection = transform.forward;
                //_rigidbody.velocity += transform.forward * _dashForce;
                _rigidbody.AddForce(transform.forward * _dashForce, ForceMode.Impulse);
            }
            
            _rigidbody.angularVelocity = Vector3.zero;
            
        }
        else
        {
            _rigidbody.velocity = new Vector3(0, _rigidbody.velocity.y, 0);
        }

        if (Input.GetButtonDown("Interact"))
        {
            _itemSelector.ItemInteract();
        }

        if (Input.GetButtonDown("Throw"))
        {
            //_itemSelector.ThrowItem(_throwingForce);
        }
    }
}