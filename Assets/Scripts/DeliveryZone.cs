﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DeliveryZone : ManagedComponent
{
    [SerializeField] private TMP_Text _boxToDeliverText;
    [SerializeField] private float _timeLimit;
    [Space]
    
    [SerializeField] private AudioSource _positiveSound;
    [SerializeField] private AudioSource _negativeSound;

    private List<int> _currentDeliveryRecipe;
    private readonly List<Box> _currentDeliveredBoxes = new List<Box>();

    private float _deliveryTimer = 0.0f;

    private void Start()
    {
        GetGameManager().GetCurrentLevel().RegisterDeliveryZone(this);
        SetCurrentDeliveryRecipe(GetGameManager().GetCurrentLevel().GenerateDeliveryRecipe());
    }

    private void OnTriggerEnter(Collider other)
    {
        Box box = other.GetComponent<Box>();
        
        if (box != null)
        {
            if (!_currentDeliveredBoxes.Contains(box) && !box.GetIsCarried())
            {
                _currentDeliveredBoxes.Add(box);
            }
        }
    }

    private void Update()
    {
        List<Box> usedBoxes = new List<Box>();
        _deliveryTimer += Time.deltaTime;
        
        foreach (int recipeItem in _currentDeliveryRecipe)
        {
            foreach (Box currentDeliveredBox in _currentDeliveredBoxes)
            {
                if (!currentDeliveredBox.GetIsCarried())
                {
                    if (currentDeliveredBox.GetDeliveryIndex() == recipeItem)
                    {
                        if (!usedBoxes.Contains(currentDeliveredBox))
                        {
                            usedBoxes.Add(currentDeliveredBox);
                            break;
                        }
                    }
                }
            }
        }

        if (_deliveryTimer > _timeLimit)
        {
            SetCurrentDeliveryRecipe(GetGameManager().GetCurrentLevel().GenerateDeliveryRecipe());
            GetGameManager().ScoreFailedRecipe();
            _negativeSound.Play();
        }
        
        if (usedBoxes.Count == _currentDeliveryRecipe.Count)
        {
            GetGameManager().ScoreRecipe(usedBoxes, _currentDeliveryRecipe);
            _positiveSound.Play();
            
            _currentDeliveredBoxes.Clear();
            
            SetCurrentDeliveryRecipe(GetGameManager().GetCurrentLevel().GenerateDeliveryRecipe());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Box box = other.GetComponent<Box>();
        
        if (box != null)
        {
            if (_currentDeliveredBoxes.Contains(box))
            {
                _currentDeliveredBoxes.Remove(box);
            }
        }
    }

    public void SetCurrentDeliveryRecipe(List<int> currentDeliveryRecipe)
    {
        string recipeText = "";
        _deliveryTimer = 0.0f;
        
        foreach (int currentDeliveryRecipeElement in currentDeliveryRecipe)
        {
            recipeText += (char) (65 + currentDeliveryRecipeElement);
        }
        
        _currentDeliveryRecipe = currentDeliveryRecipe;

        _boxToDeliverText.SetText(recipeText);
    }
}