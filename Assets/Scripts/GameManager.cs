﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public event Action<int> onScoreChanged;
    public event Action<String> onGameplayPhaseChanged;

    [SerializeField] private GuiManager guiManager;
    [SerializeField] private float _initialCrateSize = 1.5f;
    [SerializeField] private int _correctBoxScore = 100;
    [SerializeField] private int _wrongBoxScore = 0;
    [SerializeField] private int _failedRecipeScore = -50;

    private int _score = 0;
    private bool _doesTriggerNextPhase;
    private Level _level;
    private readonly Dictionary<Type, State> _states = new Dictionary<Type, State>();
    private State _currentState;
    private State _previousState;
    private readonly List<Box> _boxes = new List<Box>();

    private bool _isGameOver;
    private float _currentCrateSize;
    private readonly List<string> _levels = new List<string>();
    private string _localPlayerName;

    private void Start()
    {
        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
        SceneManager.sceneUnloaded += SceneManagerOnSceneUnloaded;
        
        InitialiseState<TitleScreenState>();
        InitialiseState<IngameState>();
        InitialiseState<LevelSelectionState>();
        InitialiseState<GameOverState>();

        _currentState = GetState<TitleScreenState>();
        _previousState = _currentState;
        _currentState.OnEnter(this);
        
        _levels.Add("level01");
        _levels.Add("level02");
        _levels.Add("level03");
        _levels.Add("level04");
        _levels.Add("level05");

        string firstLevel = _levels[0];

        if (!IsLevelUnlocked(firstLevel))
        {
            UnlockLevel(firstLevel);
        }
        
        _localPlayerName = PlayerPrefs.GetString("PLAYER_NAME", SystemInfo.deviceName);
    }

    public void Initialise()
    {
        _score = 0;
        ScoreBox(null, false);
        GetCurrentLevel().Initialise();
        
        _currentCrateSize = _initialCrateSize;
    }

    private void Update()
    {
        _currentState.OnUpdate(this);

        if (Input.GetButtonDown("Unlock All Levels"))
        {
            UnlockAllLevels();
        }
    }

    public bool IsLevelUnlocked(string levelName)
    {
        bool isLevelUnlocked = PlayerPrefs.GetInt(string.Format("{0}_IS_UNLOCKED", levelName.ToUpper())) == 1;

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            isLevelUnlocked = true;
        }
        
        return isLevelUnlocked;
    }

    public void SaveScore()
    {
        string currentLevel = _level.name;
        int previousStarCount = PlayerPrefs.GetInt(string.Format("{0}_STAR_COUNT", currentLevel.ToUpper()), 0);
        int previousScore = PlayerPrefs.GetInt(string.Format("{0}_SCORE", currentLevel.ToUpper()), 0);
        int currentLevelScore = GetScore();
        int currentLevelStarCount = GetStarCount(currentLevelScore);

        if (currentLevelStarCount > previousStarCount)
        {
            PlayerPrefs.SetInt(string.Format("{0}_STAR_COUNT", currentLevel.ToUpper()), currentLevelStarCount);
        }

        if (currentLevelScore > previousScore)
        {
            PlayerPrefs.SetInt(string.Format("{0}_SCORE", currentLevel.ToUpper()), currentLevelScore);
        }
        
        PlayerPrefs.Save();

        LeaderboardService leaderboardService = GetComponentInChildren<LeaderboardService>(true);
        StartCoroutine(leaderboardService.newScore(currentLevel.ToUpper(), _localPlayerName, currentLevelScore));
    }

    public int GetScore(string levelName)
    {
        return PlayerPrefs.GetInt(string.Format("{0}_SCORE", levelName.ToUpper()), 0);
    }

    public int GetStars(string levelName)
    {
        return PlayerPrefs.GetInt(string.Format("{0}_STAR_COUNT", levelName).ToUpper(), 0);
    }

    public void UnlockNextLevel()
    {
        string currentLevel = _level.name;
        bool doesUnlockNext = false;

        foreach (string level in _levels)
        {
            if (doesUnlockNext)
            {
                UnlockLevel(level);
                
                break;
            }
            
            if (string.Compare(level, currentLevel, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                doesUnlockNext = true;
            }
        }
    }

    private void UnlockLevel(string levelName)
    {
        PlayerPrefs.SetInt(string.Format("{0}_IS_UNLOCKED", levelName.ToUpper()), 1);
        PlayerPrefs.Save();
    }

    private void UnlockAllLevels()
    {
        foreach (string level in _levels)
        {
            UnlockLevel(level);
        }

        guiManager.UpdateLevelSelectionScreen();
    }

    public List<string> GetLevels()
    {
        return _levels;
    }

    public void ScoreFailedRecipe()
    {
        _score += _failedRecipeScore;
        
        if (onScoreChanged != null)
        {
            onScoreChanged(_score);
        }
    }

    public void ScoreRecipe(List<Box> currentDeliveredBoxes, List<int> currentDeliveryRecipe)
    {
        _score += _correctBoxScore * currentDeliveryRecipe.Count * currentDeliveryRecipe.Count;
        
        foreach (Box currentDeliveredBox in currentDeliveredBoxes)
        {
            if (currentDeliveredBox)
            {
                currentDeliveredBox.AnimateAndDestroy();
            }
        }

        _score += (currentDeliveredBoxes.Count - currentDeliveryRecipe.Count) * _wrongBoxScore;
        
        currentDeliveredBoxes.Clear();

        if (onScoreChanged != null)
        {
            onScoreChanged(_score);
        }
    }

    public void ScoreBox(Box box, bool hasMatchDeliveryIndex)
    {
        if (box != null)
        {
            if (hasMatchDeliveryIndex)
            {
                _score += 100;
            }
            else
            {
                _score += 10;
            }

            GameObject.Destroy(box.gameObject);
            _boxes.Remove(box);
        }

        if (onScoreChanged != null)
        {
            onScoreChanged(_score);
        }
    }

    public Level GetCurrentLevel()
    {
        return _level;
    }

    public int GetStarCount(int score)
    {
        int starCount = 0;
        
        if (score >= _level.GetOneStarGoal())
        {
            ++starCount;

            if (score >= _level.GetTwoStarGoal())
            {
                ++starCount;
                
                if (score >= _level.GetThreeStarGoal())
                {
                    ++starCount;
                }
            }
        }

        return starCount;
    }

    public void SetLevel(Level level)
    {
        _level = level;
    }

    public void RegisterBox(Box box)
    {
        _boxes.Add(box);
    }

    public int GetUnloadedBoxCount(int crateDeliveryIndex)
    {
        int crateCount = 0;
        
        foreach (Box box in _boxes)
        {
            if (box.GetDeliveryIndex() == crateDeliveryIndex)
            {
                ++crateCount;
            }
        }
        
        return crateCount;
    }

    public GuiManager GetGuiManager()
    {
        const bool DOES_INCLUDE_INACTIVE = true;

        return GetComponentInChildren<GuiManager>(DOES_INCLUDE_INACTIVE);
    }

    public float GetCurrentCrateSize()
    {
        return _currentCrateSize;
    }

//    public int GenerateValidDeliveryIndex()
//    {
//        int deliveryIndex = Random.Range(0, _currentDeliveryZoneRange);
//
//        while (GetUnloadedBoxCount(deliveryIndex) <= 0)
//        {
//            deliveryIndex = Random.Range(0, _currentDeliveryZoneRange);
//        }
//
//        return deliveryIndex;
//    }

    private void InitialiseState<TYPE_STATE>() where TYPE_STATE : State
    {
        Type locStateType = typeof(TYPE_STATE);
        TYPE_STATE locState = new GameObject(locStateType.ToString()).AddComponent<TYPE_STATE>();

        _states.Add(locStateType, locState);
        locState.transform.SetParent(transform, false);
    }

    private TYPE_STATE GetState<TYPE_STATE>() where TYPE_STATE : State
    {
        Type locStateType = typeof(TYPE_STATE);

        return (TYPE_STATE) _states[locStateType];
    }

    public void ChangeState<TYPE_STATE>() where TYPE_STATE : State
    {
        Type locStateType = typeof(TYPE_STATE);

        Assert.IsTrue(_states.ContainsKey(locStateType));
        Assert.AreNotEqual(_currentState.GetType(), locStateType);

        _currentState.OnExit(this);

        _previousState = _currentState;
        _currentState = GetState<TYPE_STATE>();
        _currentState.OnEnter(this);
    }

    public bool IsCurrentState<TYPE_STATE>() where TYPE_STATE : State
    {
        Type locStateType = typeof(TYPE_STATE);

        return (_currentState != null) && (_currentState.GetType() == locStateType);
    }

    public TYPE_STATE GetCurrentState<TYPE_STATE>() where TYPE_STATE : State
    {
        return _currentState as TYPE_STATE;
    }

    public void LoadScene(string sceneName)
    {
        foreach (Scene scene in SceneManager.GetAllScenes())
        {
            if (scene.name != "main")
            {
                SceneManager.UnloadSceneAsync(scene);
            }
        }

        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
    }

    private void SceneManagerOnSceneUnloaded(Scene scene)
    {
        Debug.unityLogger.Log("Scene unloaded : " + scene.name);
    }

    private void SceneManagerOnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        Debug.unityLogger.Log("Scene loaded : " + scene.name);
    }

    public int GetScore()
    {
        return _score;
    }

    public string GetLocalPlayerName()
    {
        return _localPlayerName;
    }

    public void SetLocalPlayerName(string localPlayerName)
    {
        _localPlayerName = localPlayerName;
        
        PlayerPrefs.SetString("PLAYER_NAME", _localPlayerName);
        PlayerPrefs.Save();
        
    }
}
