﻿using UnityEngine;

public class BoxSpawner : ManagedComponent
{
    [SerializeField] private int _spawnForce = 200;
    [SerializeField] private Box _boxPrefab;
    [SerializeField] private Transform _spawnDock;

    private float _timeSinceLastSpawn = 0;
    
    private void Start()
    {
        _timeSinceLastSpawn = GetGameManager().GetCurrentLevel().GetCurrentCrateSpawnInterval();
    }

    private void Update()
    {
        float currentCrateSpawnInterval = GetGameManager().GetCurrentLevel().GetCurrentCrateSpawnInterval();
        
        _timeSinceLastSpawn += Time.deltaTime;
        
        while ((currentCrateSpawnInterval > 0) && _timeSinceLastSpawn >= currentCrateSpawnInterval)
        {
            int boxDeliveryIndex = Random.Range(0, GetGameManager().GetCurrentLevel().GetCurrentDeliveryZoneRange());
            float crateSize = GetGameManager().GetCurrentCrateSize();
            Box instancedBox = GameObject.Instantiate(_boxPrefab, _spawnDock.position, _spawnDock.rotation);

            instancedBox.transform.localScale = Vector3.one * crateSize;
            instancedBox.Initialise(boxDeliveryIndex);
            instancedBox.GetComponent<Rigidbody>().AddForce(-_spawnDock.up * _spawnForce, ForceMode.Impulse);
            
            _timeSinceLastSpawn -= currentCrateSpawnInterval;
        }
    }
}
