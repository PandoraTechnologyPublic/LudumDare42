﻿using System.Collections.Generic;
using UnityEngine;

public class Level : ManagedComponent
{
    [SerializeField] private BoxSpawner[] _boxSpawner;
    [SerializeField] private PlayerSpawn _playerSpawn;
    [SerializeField] private AudioSource _levelMusic;
    [SerializeField] private float _levelDuration = 5 * 60;
    [SerializeField] private int _oneStarGoal = 1000;
    [SerializeField] private int _twoStarGoal = 10000;
    [SerializeField] private int _threeStarGoal = 20000;
    
    [Header("Recipe Complexity")]
    [SerializeField] private int _initialRecipeComplexityRange = 1;
    [SerializeField] private int _recipeComplexityRangeIncreaseParameter = 1;
    [SerializeField] private float _recipeComplexityRangeIncreaseInterval = 60.0f;
    [SerializeField] private int _maxRecipeComplexityRange = 4;

    [Header("Delivery Range")]
    [SerializeField] private int _initialDeliveryZoneRange = 1;
    [SerializeField] private int _deliveryZoneRangeIncreaseParameter = 1;
    [SerializeField] private float _deliveryZoneRangeIncreaseInterval = 60.0f;
    [SerializeField] private int _maxDeliveryZoneRange = 5;
    
    [Header("Crate Spawn Interval")]
    [SerializeField] private float _initialCrateSpawnInterval = 3.0f;
    [SerializeField] private float _crateSpawnIntervalIncreaseParameter = 0.1f;
    [SerializeField] private float _crateSpawnIntervalIncreaseInterval = 30.0f;
    [SerializeField] private float _minCrateSpawnInterval = 0.1f;
    
    private readonly List<DeliveryZone> _deliveryZones = new List<DeliveryZone>();

    private int _currentDeliveryZoneRange;
    private float _currentCrateSpawnInterval;
    private int _currentRecipeComplexityRange;
    private float _timeSinceLevelStart = 0;

    private void Awake()
    {
        GetGameManager().SetLevel(this);
        DisableCrateSpawning();
    }

    private void Update()
    {
        _timeSinceLevelStart += Time.deltaTime;
        
        CheckIncreaseDifficulty();
    }

    public void Initialise()
    {
        _currentDeliveryZoneRange = _initialDeliveryZoneRange;
        _currentCrateSpawnInterval = _initialCrateSpawnInterval;
        _currentRecipeComplexityRange = _initialRecipeComplexityRange;
        _playerSpawn.Respawn();
    }

    private void CheckIncreaseDifficulty()
    {
        int nextDeliveryZoneRange = Mathf.Min(
            _maxDeliveryZoneRange, 
            _initialDeliveryZoneRange 
            + _deliveryZoneRangeIncreaseParameter 
            * Mathf.FloorToInt(_timeSinceLevelStart / _deliveryZoneRangeIncreaseInterval)
        );
        
        if (_currentDeliveryZoneRange != nextDeliveryZoneRange)
        {
            Debug.unityLogger.Log("DeliveryZoneRange increased!");
            _currentDeliveryZoneRange = nextDeliveryZoneRange;
        }
            
        float nextCrateSpawnInterval = Mathf.Max(
            _minCrateSpawnInterval, 
            _initialCrateSpawnInterval
            - _crateSpawnIntervalIncreaseParameter
            * Mathf.FloorToInt(_timeSinceLevelStart / _crateSpawnIntervalIncreaseInterval)
        );
        
        if (_currentCrateSpawnInterval != nextCrateSpawnInterval)
        {
            Debug.unityLogger.Log("CrateSpawnInterval decreased!");
            _currentCrateSpawnInterval = nextCrateSpawnInterval;
        }
        
        int nextRecipeComplexityRange = Mathf.Min(
            _maxRecipeComplexityRange, 
            _initialRecipeComplexityRange 
            + _recipeComplexityRangeIncreaseParameter 
            * Mathf.FloorToInt(_timeSinceLevelStart / _recipeComplexityRangeIncreaseInterval)
        );
        
        if (_currentRecipeComplexityRange != nextRecipeComplexityRange)
        {
            Debug.unityLogger.Log("RecipeComplexityRange increased!");
            _currentRecipeComplexityRange = nextRecipeComplexityRange;
        }
    }

    public void EnableCrateSpawning()
    {
        foreach (BoxSpawner boxSpawner in _boxSpawner)
        {
            boxSpawner.gameObject.SetActive(true);
        }
    }

    public void DisableCrateSpawning()
    {
        foreach (BoxSpawner boxSpawner in _boxSpawner)
        {
            boxSpawner.gameObject.SetActive(false);
        }
    }
    
    public void RegisterDeliveryZone(DeliveryZone deliveryZone)
    {
        if (!_deliveryZones.Contains(deliveryZone))
        {
            _deliveryZones.Add(deliveryZone);
        }
    }

    public int GetCurrentDeliveryZoneRange()
    {
        return _currentDeliveryZoneRange;
    }

    public float GetCurrentCrateSpawnInterval()
    {
        return _currentCrateSpawnInterval;
    }

    public List<int> GenerateDeliveryRecipe()
    {
        List<int> deliveryRecipe = new List<int>();
        
        for (int deliveryIndexIndex = 0; deliveryIndexIndex < _currentRecipeComplexityRange; deliveryIndexIndex++)
        {
            deliveryRecipe.Add(Random.Range(0, _currentDeliveryZoneRange));
        }

        return deliveryRecipe;
    }

    public void SpeedupMusic()
    {
        _levelMusic.pitch = 1.3f;
    }

    public float GetLevelDuration()
    {
        return _levelDuration;
    }

    public int GetOneStarGoal()
    {
        return _oneStarGoal;
    }

    public int GetTwoStarGoal()
    {
        return _twoStarGoal;
    }

    public int GetThreeStarGoal()
    {
        return _threeStarGoal;
    }
}
