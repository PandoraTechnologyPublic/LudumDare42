using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LeaderboardService : MonoBehaviour
{
    [SerializeField] private string leaderboardUrl;

	void Start ()
    {
        //StartCoroutine(newScore("Level-1", "DeepTerror", 3000));
        //StartCoroutine(searchScores("Level-1"));
    }
	
    public IEnumerator searchScores(Action<List<Score>> callback, string level, int from = 0, int size = 10)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(leaderboardUrl + "/ld42_score/_search?q=level:'" + level + "'&sort=score:desc&from="+ from + "&size=" + size))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);

                // Deserialize JSON
                List<HitResult> results = JsonUtility.FromJson<ScoreResult>(www.downloadHandler.text).hits.hits;

                // Or retrieve results as binary data
                List<Score> scores = new List<Score>();
                foreach(HitResult result in results)
                {
                    scores.Add(result._source);
                }
                Debug.Log(JsonUtility.ToJson(scores));

                callback(scores);
            }
        }
    }

    public IEnumerator newScore(string level, string playerName, int score)
    {
        // Forge the Document
        Score newScore = new Score();
        newScore.created = DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        newScore.level = level;
        newScore.playerName = playerName;
        newScore.score = score;

        // Serialize in JSON
        String newScoreJson = JsonUtility.ToJson(newScore);
        Debug.Log(newScoreJson);

        // Forge WWWForm
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        //byte[] b = System.Text.Encoding.UTF8.GetBytes();
        byte[] json_data = System.Text.Encoding.ASCII.GetBytes(newScoreJson.ToCharArray());
        ///POST by IIS hosting...
        WWW api = new WWW(leaderboardUrl + "/ld42_score/_doc", json_data, headers);

        yield return api;

        Debug.Log(api.text);
    }

    [Serializable]
    public class Score
    {
        public string playerName;
        public string level;
        public int score;
        public double created;
    }

    [Serializable]
    public class ScoreResult
    {
        public HitResults hits;
    }

    [Serializable]
    public class HitResults
    {
        public List<HitResult> hits;
    }

    [Serializable]
    public class HitResult
    {
        public Score _source;
    }
}

