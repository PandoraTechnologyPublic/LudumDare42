﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelButtonLayout : MonoBehaviour
{
    [SerializeField] private TMP_Text _levelNameText;
    [SerializeField] private RectTransform _bestScorePanel;
    [SerializeField] private TMP_Text _bestScoreText;
    [SerializeField] private RectTransform _starsPanel;
    [SerializeField] private Image _oneStarImage;
    [SerializeField] private Image _twoStarImage;
    [SerializeField] private Image _threeStarImage;

    private void Awake()
    {
        _starsPanel.gameObject.SetActive(false);
        _bestScorePanel.gameObject.SetActive(false);
    }

    public void SetLevelName(string levelName)
    {
        _levelNameText.SetText(levelName);    
    }
    
    public void DisplayStars(int starCount)
    {
        _oneStarImage.gameObject.SetActive(false);
        _twoStarImage.gameObject.SetActive(false);
        _threeStarImage.gameObject.SetActive(false);
        
        if (starCount >= 1)
        {
            _starsPanel.gameObject.SetActive(true);
            _oneStarImage.gameObject.SetActive(true);
            
            if (starCount >= 2)
            {
                _twoStarImage.gameObject.SetActive(true);
                
                if (starCount >= 3)
                {
                    _threeStarImage.gameObject.SetActive(true);
                }
            }
        }
    }

    public void DisplayScore(int score)
    {
        if (score > 0)
        {
            _bestScorePanel.gameObject.SetActive(true);
            _bestScoreText.SetText(string.Format("Best : {0}", score));
        }
    }
}