﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Animator))]
public class Box : Interactable
{
    [SerializeField] private List<TMP_Text> _texts;
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private Transform _boxHighlight;
    [SerializeField] private AudioSource _interactionSound;
    [SerializeField] private int _deliveryIndex;
    
    private bool _isCarried = false;

    private Animator _animator;

    private float _mass;

    public void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void Start()
    {
        _mass = _rigidbody.mass;
        _canGrab = true;
        _boxHighlight.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        Initialise(_deliveryIndex);
    }

    public bool GetIsCarried()
    {
        return _isCarried;
    }

    private void Update()
    {
        #if UNITY_EDITOR
            Initialise(_deliveryIndex);
        #endif
    }

    public override void Interact()
    {
        if (_isCarried)
        {
            Release();
        }
        else
        {
            Grab();
        }
    }

    public override float GetItemMass()
    {
        return _mass;
    }

    public void Grab()
    {
        _isCarried = true;
        _rigidbody.velocity = Vector3.zero;
        ToggleHighlight(false);
        GameObject.Destroy(_rigidbody);
        _interactionSound.Play();
    }

    public void Release()
    {
        _isCarried = false;
        _rigidbody = gameObject.AddComponent<Rigidbody>();
        _rigidbody.mass = _mass;
        ToggleHighlight(true);
        ThrowItem(20);
    }

    public override void ToggleHighlight(bool toggle)
    {
        if (toggle)
        {
            _boxHighlight.gameObject.SetActive(true);
        }
        else
        {
            _boxHighlight.gameObject.SetActive(false);
        }
    }

    public override void ThrowItem(float force)
    {
        _rigidbody.AddForce(transform.forward * force, ForceMode.Impulse);
    }

    public void Initialise(int boxDeliveryIndex)
    {
        char boxCharacter = (char) (65 + boxDeliveryIndex);
        
        _deliveryIndex = boxDeliveryIndex;
        
        GetGameManager().RegisterBox(this);

        foreach (TMP_Text crateText in _texts)
        {
            crateText.SetText(boxCharacter.ToString());
        }
    }
    
    public int GetDeliveryIndex()
    {
        return _deliveryIndex;
    }

    public void AnimateAndDestroy()
    {
        _canGrab = false;
        _rigidbody.isKinematic = true;
        _rigidbody.velocity = Vector3.zero;
        if (_animator)
        {
            _animator.enabled = true;
            _animator.Play("BoxValidation");
        }
        GetComponent<BoxCollider>().enabled = false;

        GameObject.Destroy(gameObject, 0.3f);
    }
}
