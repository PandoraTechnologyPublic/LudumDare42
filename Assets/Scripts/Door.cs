﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Door : Interactable
{
	[SerializeField] private bool _timeRuled = true;
	[SerializeField] private bool _interactable = false;
	[SerializeField] private bool _autoClose = true;
	[SerializeField] private float _openInterval = 5f;
	[SerializeField] private float _closeInterval = 5f;
	
	private Animator _animator;
	private Renderer _renderers;
	private Color _color;

	private float _time = 0f;
	private bool _open = false;
	
	// Use this for initialization
	void Start ()
	{
		_animator = GetComponent<Animator>();
		_renderers = GetComponentInChildren<Renderer>();
		_color = _renderers.material.color;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (_timeRuled || _autoClose)
		{
			_time += Time.deltaTime;
		}
		
		if (_timeRuled)
		{
			if (_open && _closeInterval < _time ||
			    !_open && _openInterval < _time)
			{
				_animator.SetTrigger("Trigger");
				_open = !_open;
				_time = 0f;
			}
		}
		else if (_autoClose)
		{
			if (_open && _closeInterval < _time)
			{
				_animator.SetTrigger("Trigger");
				_open = !_open;
				_time = 0f;
			}
		}
	}

	public override void Interact()
	{
		if (_interactable)
		{
			_animator.SetTrigger("Trigger");
			_open = !_open;
			if (_autoClose)
			{
				_time = 0f;
			}
		}
	}

	public override void ToggleHighlight(bool toggle)
	{
		if (_interactable)
		{
			if (toggle)
			{
				_renderers.material.color = Color.yellow;
			}
			else
			{
				_renderers.material.color = _color;
			}
		}
	}
}
