﻿using UnityEngine;

public class VelocitySound : MonoBehaviour
{
    public float minVolume = 0.3f;
    public float maxVolume = 0.6f;
    public float minPitch = 0.6f;
    public float maxPitch = 1.8f;

    public float mult = 3.5f;

    private float velocity;
    private float oldVelocity;
    private AudioSource audio;
    
	void Start ()
    {
        audio = this.GetComponent<AudioSource>();
	}
	
	void Update ()
    {
        velocity = this.GetComponentInParent<Rigidbody>().velocity.magnitude * Time.deltaTime * mult;

        if (velocity < oldVelocity)
        {
            velocity = Mathf.Lerp(oldVelocity, velocity, Time.deltaTime*3f);
        }

        audio.volume = Mathf.Lerp(minVolume, maxVolume, velocity);
        audio.pitch = Mathf.Lerp(minPitch, maxPitch, velocity);

        oldVelocity = velocity;
	}
}
