﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour
{
	private float _speed;
	private Rigidbody _rigidbody;

	void Awake()
	{
		_rigidbody = GetComponent<Rigidbody>();
	}
	
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		Vector3 movement = transform.forward * _speed * Time.deltaTime;

		_rigidbody.position -= movement;
		_rigidbody.MovePosition(_rigidbody.position + movement);
	}

	public void SetSpeed(float speed)
	{
		_speed = speed;
	}

	public float GetSpeed()
	{
		return _speed;
	}
}
