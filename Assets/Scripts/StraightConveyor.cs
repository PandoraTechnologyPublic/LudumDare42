﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class StraightConveyor : MonoBehaviour
{
    [SerializeField] private bool _hasRandomOrientation;
    [SerializeField] private float _beltSpeed;
    [SerializeField] private ConveyorBelt _conveyorBelt;

    private Animator _animator;

    private void Start()
    {
        GetComponent<Animator>().speed = _beltSpeed;
        _conveyorBelt.SetSpeed(_beltSpeed);

        if (_hasRandomOrientation)
        {
            transform.localRotation = Quaternion.Euler(0, 90 * Random.Range(0, 4), 0);
        }
    }

    private void Update()
    {
        _conveyorBelt.SetSpeed(_beltSpeed);
    }
}
